﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Media;
using Edge360.Platform.VMS.MonitoringLibrary;
using Edge360.Platform.VMS.MonitoringLibrary.Models;
using Edge360.Platform.VMS.MonitoringLibrary.Models.TelemetryDescriptors;
using Edge360.Platform.VMS.TelemetryControl.DataObject;

namespace Edge360.Platform.VMS.TelemetryControl.ViewModel
{
    public class TelemetryControlViewModel : DependencyObject, INotifyPropertyChanged
    {
        public static Action<TelemetryAlert> TelemetryAlert;
        private TelemetryStats _averageStats;
        private Timer _logTimer;
        private TelemetryQueue<TelemetryStats> _statsQueue;


        public TelemetryControlViewModel(IconDetails icons, ColorDetails colors, LoggingDetails logging,
            ThresholdDetails threshold)
        {
            Icons = icons;
            Colors = colors;
            LoggingInfo = logging;
            Thresholds = threshold;

            MonitoringAgent.InitializeLogger();
            AdjustTooltipDuration();
            StartLogging();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void StartLogging()
        {
            _statsQueue = new TelemetryQueue<TelemetryStats>(LoggingInfo.QueueLimit);
            _logTimer = new Timer(state => Logging_Timer(), null, TimeSpan.Zero,
                TimeSpan.FromSeconds(LoggingInfo.Interval));
        }

        private void Logging_Timer()
        {
            UpdateStats();
        }

        private async void UpdateStats()
        {
            try
            {
                var stats = await MonitoringAgent.CollectTelemetryStats();
                _statsQueue.Enqueue(stats);
                _averageStats = await _statsQueue.GetAverageStats();
                NotifyUtilizationUpdates();
                SetTelemetryStatus();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        private void SetTelemetryStatus()
        {
            var highestPercentage = _averageStats.GetHighestPercentage();
            var gpuPercentage = _averageStats.GetGpuHighestPercentage();
            try
            {
                //Application.Current.Dispatcher?.Invoke( async() =>
               // {
                    if (highestPercentage > Thresholds.Highest || gpuPercentage > GpuThreshold.Highest)
                    {
                        TelemetryAlert?.Invoke(new TelemetryAlert(MonitoringAgent.CurrentStats));
                        SetTachometerIcon(Icons.Highest, Colors.High);
                    }
                    else if (highestPercentage > Thresholds.High  || gpuPercentage > GpuThreshold.High)
                    {
                        TelemetryAlert?.Invoke(new TelemetryAlert(MonitoringAgent.CurrentStats));
                        SetTachometerIcon(Icons.High, Colors.High);
                    }
                    else if (highestPercentage > Thresholds.Average  || gpuPercentage > GpuThreshold.Average)
                    {
                        TelemetryAlert?.Invoke(new TelemetryAlert(MonitoringAgent.CurrentStats));
                        SetTachometerIcon(Icons.Average, Colors.Average);
                    }
                    else if (highestPercentage > Thresholds.Low  || gpuPercentage > GpuThreshold.Low)
                    {
                        TelemetryAlert?.Invoke(new TelemetryAlert(MonitoringAgent.CurrentStats));
                        SetTachometerIcon(Icons.Low, Colors.Low);
                    }
                    else
                    {
                        TelemetryAlert?.Invoke(new TelemetryAlert(MonitoringAgent.CurrentStats));
                        SetTachometerIcon(Icons.Lowest, Colors.Low);
                    }
               // });
            }
            catch (Exception e)
            {
            }
        }

        private void SetTachometerIcon(string icon, string color)
        {
            CurrentTachIcon = icon;
            CurrentTachColor = (SolidColorBrush) new BrushConverter().ConvertFromString(color);
        }

        private static void AdjustTooltipDuration(int duration = int.MaxValue)
        {
            //ToolTipService.ShowDurationProperty.OverrideMetadata(typeof(DependencyObject),
            //    new FrameworkPropertyMetadata(duration));
        }

        private void NotifyUtilizationUpdates()
        {
            new List<string>
                    {"DiskUtilization", "NetworkUtilization", "GpuUtilization", "CpuUtilization", "MemoryUtilization"}
                .ForEach(NotifyPropertyChanged);
        }

        private void NotifyPropertyChanged(string property)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }

        #region Shared Variables

        public IconDetails Icons { get; }
        public ColorDetails Colors { get; }
        public LoggingDetails LoggingInfo { get; }
        public ThresholdDetails Thresholds { get; }
        public ThresholdDetails GpuThreshold { get; set; } = new ThresholdDetails(70, 55, 35, 10);
        #endregion

        #region Binding Variables

        private string _currentTachIcon = "\uf624";

        public string CurrentTachIcon
        {
            get => _currentTachIcon;
            set
            {
                if (_currentTachIcon == value)
                {
                    return;
                }

                _currentTachIcon = value;
                NotifyPropertyChanged("CurrentTachIcon");
            }
        }

        private SolidColorBrush _currentTachColor = (SolidColorBrush) new BrushConverter().ConvertFromString("Black");

        public SolidColorBrush CurrentTachColor
        {
            get => _currentTachColor;
            set
            {
                if (_currentTachColor == value)
                {
                    return;
                }

                _currentTachColor = value;
                NotifyPropertyChanged("CurrentTachColor");
            }
        }

        public BindingList<TelemetryRecording> DiskUtilization => _averageStats?.DiskUtilization;
        public BindingList<TelemetryRecording> NetworkUtilization => _averageStats?.NetworkUtilization;
        public BindingList<TelemetryRecording> GpuUtilization => _statsQueue.LastOrDefault()?.GpuUtilization;
        public BindingList<TelemetryRecording> GpuTemps => _statsQueue.LastOrDefault()?.GpuTemps;
        public TelemetryRecording CpuUtilization => _averageStats?.CpuUtilization;
        public TelemetryRecording MemoryUtilization => _averageStats?.MemoryUtilization;

        #endregion
    }
}