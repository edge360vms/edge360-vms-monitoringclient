﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using Edge360.Platform.VMS.MonitoringLibrary.Models;

namespace Edge360.Platform.VMS.TelemetryControl.Resources.Converters
{
    public class MultiListVisibilityConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            var bindingListType = typeof(BindingList<TelemetryRecording>);
            try
            {
                if (values[0]?.GetType() != bindingListType || values[1]?.GetType() != bindingListType)
                {
                    return Visibility.Visible;
                }

                var stats = values.Select(x => x as BindingList<TelemetryRecording>).ToList();
                return stats.All(stat => stat.Count == 0) ? Visibility.Collapsed : Visibility.Visible;
            }
            catch
            {
                return Visibility.Collapsed;
            }
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}