﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using Edge360.Platform.VMS.MonitoringLibrary.Models;

namespace Edge360.Platform.VMS.TelemetryControl.Resources.Converters
{
    public class ListVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                if (value?.GetType() != typeof(BindingList<TelemetryRecording>))
                {
                    return Visibility.Visible;
                }

                var list = (BindingList<TelemetryRecording>) value;
                return list.Count == 0 ? Visibility.Collapsed : Visibility.Visible;
            }
            catch
            {
                return Visibility.Collapsed;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}