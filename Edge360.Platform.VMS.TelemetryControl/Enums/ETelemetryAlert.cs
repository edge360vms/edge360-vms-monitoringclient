﻿namespace Edge360.Platform.VMS.TelemetryControl.Enums
{
    public enum ETelemetryAlert
    {
        HighDiskUsage,
        HighNetworkUsage,
        HighGpuUsage,
        HighCpuUsage,
        HighMemoryUsage
    }
}