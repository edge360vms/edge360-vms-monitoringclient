﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows.Controls;
using Edge360.Platform.VMS.MonitoringLibrary.Models.TelemetryDescriptors;
using Edge360.Platform.VMS.TelemetryControl.Properties;
using Edge360.Platform.VMS.TelemetryControl.ViewModel;

namespace Edge360.Platform.VMS.TelemetryControl.View
{
    /// <summary>
    ///     Interaction logic for TelemetryControl.xaml
    /// </summary>
    public partial class TelemetryControl : UserControl, INotifyPropertyChanged
    {
        public static TelemetryControlViewModel Instance { get; set; }

        public TelemetryControl()
        {
            InitializeComponent();
            DataContext = Instance ?? GetDataContext();
        }

        //#region DP

        //private static readonly DependencyProperty HighestThresholdProperty = DependencyProperty.Register(
        //    "HighestThreshold", typeof(double), typeof(TelemetryControl), new PropertyMetadata(95.0));

        //private static readonly DependencyProperty HighThresholdProperty = DependencyProperty.Register(
        //    "HighThreshold", typeof(double), typeof(TelemetryControl), new PropertyMetadata(75.0));

        //private static readonly DependencyProperty AverageThresholdProperty = DependencyProperty.Register(
        //    "AverageThreshold", typeof(double), typeof(TelemetryControl), new PropertyMetadata(50.0));

        //private static readonly DependencyProperty LowThresholdProperty = DependencyProperty.Register(
        //    "LowThreshold", typeof(double), typeof(TelemetryControl), new PropertyMetadata(25.0));

        //private static readonly DependencyProperty LoggingIntervalProperty = DependencyProperty.Register(
        //    "LoggingInterval", typeof(double), typeof(TelemetryControl), new PropertyMetadata(1.0));

        //private static readonly DependencyProperty QueueLimitProperty = DependencyProperty.Register(
        //    "QueueLimit", typeof(int), typeof(TelemetryControl), new PropertyMetadata(5));

        //private static readonly DependencyProperty HighestIconProperty = DependencyProperty.Register(
        //    "HighestIcon", typeof(string), typeof(TelemetryControl), new PropertyMetadata("\uf626"));

        //private static readonly DependencyProperty HighIconProperty = DependencyProperty.Register(
        //    "HighIcon", typeof(string), typeof(TelemetryControl), new PropertyMetadata("\uf625"));

        //private static readonly DependencyProperty AverageIconProperty = DependencyProperty.Register(
        //    "AverageIcon", typeof(string), typeof(TelemetryControl), new PropertyMetadata("\uf624"));

        //private static readonly DependencyProperty LowIconProperty = DependencyProperty.Register(
        //    "LowIcon", typeof(string), typeof(TelemetryControl), new PropertyMetadata("\uf627"));

        //private static readonly DependencyProperty LowestIconProperty = DependencyProperty.Register(
        //    "LowestIcon", typeof(string), typeof(TelemetryControl), new PropertyMetadata("\uf628"));

        //private static readonly DependencyProperty HighColorProperty = DependencyProperty.Register(
        //    "HighColor", typeof(string), typeof(TelemetryControl), new PropertyMetadata("Red"));

        //private static readonly DependencyProperty AverageColorProperty = DependencyProperty.Register(
        //    "AverageColor", typeof(string), typeof(TelemetryControl), new PropertyMetadata("Yellow"));

        //private static readonly DependencyProperty LowColorProperty = DependencyProperty.Register(
        //    "LowColor", typeof(string), typeof(TelemetryControl), new PropertyMetadata("Green"));

        //private static double _highestThreshold;

        //#endregion

        public event PropertyChangedEventHandler PropertyChanged;

        public static void InitializeTelemetry()
        {
            Task.Run(async () => { GetDataContext(); });
        }

        private static TelemetryControlViewModel GetDataContext()
        {
            var colors = new ColorDetails(HighColor, AverageColor, LowColor);
            var thresholds = new ThresholdDetails(HighestThreshold, HighThreshold, AverageThreshold, LowThreshold);
            var loggingInfo = new LoggingDetails(LoggingInterval, QueueLimit);
            var icons = new IconDetails(HighestIcon, HighIcon, AverageIcon, LowIcon, LowestIcon);

            var vm = new TelemetryControlViewModel(icons, colors, loggingInfo, thresholds);
            Instance = vm;
            return vm;
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #region DP Variables

        //public static readonly DependencyProperty MeterFontSizeProperty = DependencyProperty.Register(
        //    "MeterFontSize", typeof(int), typeof(TelemetryControl), new PropertyMetadata(20));

        public static int MeterFontSize { get; set; } = 20;

        public static double HighestThreshold { get; set; } = 95.0;
        public static double HighThreshold { get; set; } = 70.0;

        public static double AverageThreshold { get; set; } = 50.0;

        public static double LowThreshold { get; set; } = 25.0;

        public static double LoggingInterval { get; set; } = 1.0;

        public static int QueueLimit { get; set; } = 5;

        public static string HighestIcon { get; set; } = "\uf626";

        public static string HighIcon { get; set; } = "\uf625";

        public static string AverageIcon { get; set; } = "\uf624";

        public static string LowIcon { get; set; } = "\uf627";

        public static string LowestIcon { get; set; } = "\uf628";

        public static string HighColor { get; set; } = "Red";

        public static string AverageColor { get; set; } = "Yellow";

        public static string LowColor { get; set; } = "Green";

        #endregion
    }
}