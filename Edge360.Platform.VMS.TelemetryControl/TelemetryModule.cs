﻿using Prism.Ioc;
using Prism.Modularity;
using Prism.Regions;
using Prism.Unity;
using Unity;

namespace Edge360.Platform.VMS.TelemetryControl
{
    public class TelemetryModule : IModule
    {
        public void RegisterTypes(IContainerRegistry containerRegistry)
        {
        }

        public void OnInitialized(IContainerProvider containerProvider)
        {
            var regionManager = containerProvider.Resolve<IRegionManager>();
            regionManager.RegisterViewWithRegion("TelemetryModule", typeof(View.TelemetryControl));
        }

        //public void OnInitialized(IContainerProvider containerProvider, IRegionManager regionManager = null)
        //{
        //   // var regionManager = new RegionManager();// containerProvider.GetContainer().Resolve<IRegionManager>();
           
        //}
    }
}