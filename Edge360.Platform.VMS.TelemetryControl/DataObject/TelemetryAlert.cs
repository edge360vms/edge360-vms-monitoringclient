﻿using System.Linq;
using Edge360.Platform.VMS.MonitoringLibrary.Models;
using Edge360.Platform.VMS.TelemetryControl.Enums;

namespace Edge360.Platform.VMS.TelemetryControl.DataObject
{
    public class TelemetryAlert
    {
        public ETelemetryAlert AlertType { get; set; }

        public string Description { get; set; }
        public double HighestPercentage { get; set; }
        public double GpuUsage { get; set; }

        public TelemetryAlert(TelemetryStats stats)
        {
            if (stats != null)
            {
                HighestPercentage = stats.GetHighestPercentage();
                GpuUsage = stats.GetGpuHighestPercentage();
                var highestStat = stats.CpuUtilization.Value;
                AlertType = ETelemetryAlert.HighCpuUsage;
                if (stats.DiskUtilization.FirstOrDefault(o => o.Value > highestStat)?.Value is double diskUsage)
                {
                    highestStat = diskUsage;
                    AlertType = ETelemetryAlert.HighDiskUsage;
                }

                if (stats.GpuUtilization.FirstOrDefault(o => o.Value > highestStat)?.Value is double gpuUsage)
                {
                    highestStat = gpuUsage;
                    AlertType = ETelemetryAlert.HighGpuUsage;
                }

                if (stats.MemoryUtilization.Value > highestStat)
                {
                    highestStat = stats.MemoryUtilization.Value;
                    AlertType = ETelemetryAlert.HighMemoryUsage;
                }

                if (stats.NetworkUtilization.FirstOrDefault(o => o.Value > highestStat)?.Value is double netUsage)
                {
                    highestStat = netUsage;
                    AlertType = ETelemetryAlert.HighNetworkUsage;
                }

                Description = CreateDescription(highestStat, AlertType);
            }
        }

        private string CreateDescription(double highestStat, ETelemetryAlert alertType)
        {
            switch (alertType)
            {
                case ETelemetryAlert.HighDiskUsage:
                    return $"Warning High Disk Usage! ({highestStat}%)";
                case ETelemetryAlert.HighNetworkUsage:
                    return $"Warning High Network Usage! ({highestStat}%)";
                case ETelemetryAlert.HighGpuUsage:
                    return $"Warning High GPU Usage! ({highestStat}%)";
                case ETelemetryAlert.HighCpuUsage:
                    return $"Warning High CPU Usage! ({highestStat}%)";
                case ETelemetryAlert.HighMemoryUsage:
                    return $"Warning High Memory Usage! ({highestStat}%)";
            }

            return default;
        }
    }
}