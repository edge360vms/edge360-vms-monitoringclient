﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Edge360.Platform.VMS.MonitoringLibrary.Models;
using NvAPIWrapper.GPU;
using NvAPIWrapper.Native.GPU;

namespace Edge360.Platform.VMS.MonitoringLibrary.ArchitectureMonitors.GPU
{
    public static class NvidiaGpu
    {
        private static List<PhysicalGPU> _physicalGpus;

        public static async Task< BindingList<TelemetryRecording>> GetGpuUtilization()
        {
            try
            {
                var gpuStats = await Task.Run(async () =>
                {
                    //var tccGpus = PhysicalGPU.GetTCCPhysicalGPUs().ToList();
                    //var list = PhysicalGPU.GetPhysicalGPUs().ToList();
                    _physicalGpus ??= PhysicalGPU.GetPhysicalGPUs().ToList();

                    return _physicalGpus.Select(x => new TelemetryRecording()
                    {
                        Instance = x.FullName,
                        Value = Math.Max(x.UsageInformation.VideoEngine.Percentage, x.UsageInformation.GPU.Percentage) ,
                        Type = RecordingType.Percentage
                    }).ToList();
                });
                return new BindingList<TelemetryRecording>(gpuStats);
            }
            catch (Exception e)
            {
                return default;
            }
        }

        public static BindingList<TelemetryRecording> GetGpuTemp()
        {
            try
            {
                var temps = new List<TelemetryRecording>();
                _physicalGpus ??= PhysicalGPU.GetPhysicalGPUs().ToList();
                foreach (var gpu in _physicalGpus)
                {
                    var thermalSensors = gpu.ThermalInformation.ThermalSensors.ToList().Select(x => new TelemetryRecording()
                    {
                        Instance = $"{gpu.FullName} {x.Controller} Temp",
                        Value = x.CurrentTemperature,
                        Type = RecordingType.Temperature
                    }).ToList();

                    temps.AddRange(thermalSensors);
                }

                return new BindingList<TelemetryRecording>(temps);
            }
            catch (Exception e)
            {
                return default;
            }
        }
    }
}
