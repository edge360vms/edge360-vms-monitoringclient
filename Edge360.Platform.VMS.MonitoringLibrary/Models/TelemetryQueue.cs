﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Edge360.Platform.VMS.MonitoringLibrary.Models;

namespace Edge360.Platform.VMS.MonitoringLibrary.Models
{
    public class TelemetryQueue<T> : Queue<TelemetryStats>
    {
        private double Limit { get;  }

        public TelemetryQueue(double limit)
        {
            Limit = limit;
        }

        public new void Enqueue(TelemetryStats stats)
        {
            if (Count >= Limit)
            {
                Dequeue();
            }

            base.Enqueue(stats);
        }

        public async Task<TelemetryStats> GetAverageStats()
        {
            try
            {
                return await Task.Run(() =>
                {
                    return new TelemetryStats()
                    {
                        DiskUtilization = CalculateAverage(this.Select(x => x?.DiskUtilization).ToList()),
                        NetworkUtilization = CalculateAverage(this.Select(x => x?.NetworkUtilization).ToList()),
                        GpuUtilization = TakeMax(this.Select(x => x?.GpuUtilization).ToList()),
                        GpuTemps = CalculateAverage(this.Select(x => x?.GpuTemps).ToList(), RecordingType.Temperature),
                        MemoryUtilization = CalculateAverage(this.Select(x => x?.MemoryUtilization).ToList()),
                        CpuUtilization = CalculateAverage(this.Select(x => x?.CpuUtilization).ToList())
                    };
                });
            }
            catch (Exception e)
            {
                return default;
            }

        }


        private BindingList<TelemetryRecording> TakeMax(List<BindingList<TelemetryRecording>> stats, RecordingType type = RecordingType.Percentage)
        {
            try
            {
                var avgStats = stats.SelectMany(o => o).OrderBy(o => o.Value).LastOrDefault();

                return new BindingList<TelemetryRecording>(new List<TelemetryRecording>(){avgStats});
            }
            catch
            {
                return new BindingList<TelemetryRecording>();
            }
        }

        private BindingList<TelemetryRecording> CalculateAverage(List<BindingList<TelemetryRecording>> stats, RecordingType type = RecordingType.Percentage)
        {
            try
            {
                var avgStats = stats.SelectMany(x => x).Where(x => x != null).ToList().GroupBy(s => s.Instance).Select(g => new TelemetryRecording()
                {
                    Instance = g.Key,
                    Value = g.Sum(i => i.Value) / g.Count(),
                    Type = type
                }).ToList();

                return new BindingList<TelemetryRecording>(avgStats);
            }
            catch
            {
                return new BindingList<TelemetryRecording>();
            }
        }

        private TelemetryRecording CalculateAverage(List<TelemetryRecording> stats, RecordingType type = RecordingType.Percentage)
        {
            try
            {
                var count = stats.Count(stat => stat != null);
                if (count == 0)
                {
                    return null;
                }

                var avgStats = new TelemetryRecording()
                {
                    Instance = stats.First(x => x!= null).Instance,
                    Value = stats.Where(x => x != null).Sum(x => x.Value) / count,
                    Type = type
                };

                return avgStats;
            }
            catch
            {
                return null;
            }
            
        }
    }
}
