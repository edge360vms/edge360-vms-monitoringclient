﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Edge360.Platform.VMS.MonitoringLibrary.Models.TelemetryDescriptors
{
    public class ColorDetails
    {
        public string High { get; }
        public string Average { get; }
        public string Low { get; }

        public ColorDetails(string high, string average, string low)
        {
            High = high;
            Average = average;
            Low = low;
        }
    }
}
