﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Edge360.Platform.VMS.MonitoringLibrary.Models.TelemetryDescriptors
{
    public class ThresholdDetails
    {
        
        public double Highest { get; }
        public double High { get; }
        public double Average { get; }
        public double Low { get; }

        public ThresholdDetails(double highest, double high, double average, double low)
        {
            Highest = highest;
            High = high;
            Average = average;
            Low = low;
        }
    }
}
