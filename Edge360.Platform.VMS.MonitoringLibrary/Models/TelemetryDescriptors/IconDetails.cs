﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Edge360.Platform.VMS.MonitoringLibrary.Models.TelemetryDescriptors
{
    public class IconDetails
    {
        public string Highest { get; }
        public string High { get; }
        public string Average { get; }
        public string Low { get; }
        public string Lowest { get; }

        public IconDetails(string highest, string high, string average, string low, string lowest)
        {
            Highest = highest;
            High = high;
            Average = average;
            Low = low;
            Lowest = lowest;
        }
    }
}
