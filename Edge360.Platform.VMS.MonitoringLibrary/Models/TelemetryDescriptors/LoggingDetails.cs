﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Edge360.Platform.VMS.MonitoringLibrary.Models.TelemetryDescriptors
{
    public class LoggingDetails
    {
        public double Interval;
        public int QueueLimit;

        public LoggingDetails(double interval, int limit)
        {
            Interval = interval;
            QueueLimit = limit;
        }
    }
}
