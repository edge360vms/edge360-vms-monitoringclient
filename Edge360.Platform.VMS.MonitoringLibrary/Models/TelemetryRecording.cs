﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Edge360.Platform.VMS.MonitoringLibrary.Models
{
    public enum RecordingType
    {
        Percentage = 0,
        Temperature = 1
    }

    public class TelemetryRecording
    {

        public string Instance { get; set; }
        public double Value { get; set; }
        public RecordingType Type { get; set; }

        public override string ToString()
        {
            switch (Type)
            {
                case RecordingType.Percentage:
                    return string.IsNullOrEmpty(Instance)
                        ? $"{Value:0.00}%"
                        : $"{Instance} - {Value:0.00}%";
                case RecordingType.Temperature:
                    return string.IsNullOrEmpty(Instance)
                        ? $"{Value:0}°C"
                        : $"{Instance} - {Value:0}°C";
            }

            return "";
        }

        //public override bool Equals(object o)
        //{
        //    if (o == null || o.GetType() != typeof(TelemetryRecording))
        //    {
        //        return false;
        //    }

        //    var recording = (TelemetryRecording) o;

        //    return Instance == recording.Instance && Value.Equals(recording.Value) && Type == recording.Type;
        //}

        //public override int GetHashCode()
        //{
        //    unchecked
        //    {
        //        var hashCode = (Instance != null ? Instance.GetHashCode() : 0);
        //        hashCode = (hashCode * 397) ^ Value.GetHashCode();
        //        hashCode = (hashCode * 397) ^ (int) Type;
        //        return hashCode;
        //    }
        //}
    }

}
