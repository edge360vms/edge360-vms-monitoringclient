﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace Edge360.Platform.VMS.MonitoringLibrary.Models
{
    public class TelemetryStats
    {
        public TelemetryRecording CpuUtilization { get; set; }
        public BindingList<TelemetryRecording> DiskUtilization { get; set; }
        public BindingList<TelemetryRecording> GpuTemps { get; set; }
        public BindingList<TelemetryRecording> GpuUtilization { get; set; }
        public TelemetryRecording MemoryUtilization { get; set; }
        public BindingList<TelemetryRecording> NetworkUtilization { get; set; }

        private IEnumerable<TelemetryRecording> AllStats
        {
            get
            {
                var allStats = new List<TelemetryRecording>();

                if (CpuUtilization != null)
                {
                    allStats.Add(CpuUtilization);
                }

                if (MemoryUtilization != null)
                {
                    allStats.Add(MemoryUtilization);
                }

                if (GpuUtilization != null)
                {
                    allStats.AddRange(GpuUtilization);
                }

                if (DiskUtilization != null)
                {
                    allStats.AddRange(DiskUtilization);
                }

                if (NetworkUtilization != null)
                {
                    allStats.AddRange(NetworkUtilization);
                }


                return allStats;
            }
        }

        public double GetGpuHighestPercentage()
        {
            try
            {
                return GpuUtilization.Max(o => o.Value);
            }
            catch (Exception e)
            {
                return 0;
            }
        }

        public double GetHighestPercentage()
        {
            try
            {
                return AllStats.Max(elem => elem.Value);
            }
            catch
            {
                return 0.0;
            }
        }
    }
}