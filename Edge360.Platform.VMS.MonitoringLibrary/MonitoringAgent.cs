﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Edge360.Platform.VMS.MonitoringLibrary.ArchitectureMonitors.GPU;
using Edge360.Platform.VMS.MonitoringLibrary.Models;
using Microsoft.VisualBasic.Devices;
using NvAPIWrapper;

namespace Edge360.Platform.VMS.MonitoringLibrary
{
    internal enum HardwareType
    {
        Unknown = 0,
        Nvidia = 1,
        Amd = 2,
    }

    public class MonitoringAgent
    {
        private static ulong TotalMemory { get; set; }
        private static HardwareType GpuType { get; set; }

        private static PerformanceCounter CpuUsage { get; set; }
        private static List<PerformanceCounter> DiskUsage { get; set; }
        private static PerformanceCounter AvailableMem { get; set; }
        private static List<PerformanceCounter> NetworkBandwidth { get; set; }
        private static List<PerformanceCounter> NetworkUsage { get; set; }

        public static void InitializeLogger()
        {
            InitializePerformanceCounterLists();
            InitializeCounters();
        }

        private static void InitializePerformanceCounterLists()
        {
            try
            {
                TotalMemory = ((new ComputerInfo()).TotalPhysicalMemory) / 1024 / 1024;
            }
            catch (Exception e)
            {
                Trace.WriteLine(e);
            }

            try
            {
                GetGpuType();
            }
            catch (Exception e)
            {
                Trace.WriteLine(e);
            }

            try
            {
                CpuUsage = new PerformanceCounter("Processor Information", "% Processor Time", "_Total");
            }
            catch (Exception e)
            {
                Trace.WriteLine(e);
            }

            try
            {
                AvailableMem = new PerformanceCounter("Memory", "Available MBytes");
            }
            catch (Exception e)
            {
                Trace.WriteLine(e);
            }

            try
            {
                var diskInstanceNames = new PerformanceCounterCategory("PhysicalDisk").GetInstanceNames().ToList();
                DiskUsage = diskInstanceNames
                    .Select(instance => new PerformanceCounter("PhysicalDisk", "% Disk Time", instance)).Where(instance => instance.InstanceName != "_Total").ToList();
            }
            catch (Exception e)
            {
                Trace.WriteLine(e);
            }

            try
            {
                var networkInstanceNames = new PerformanceCounterCategory("Network Interface").GetInstanceNames().ToList();
                NetworkBandwidth = networkInstanceNames.Select(instance =>
                    new PerformanceCounter("Network Interface", "Current Bandwidth", instance)).ToList();
                NetworkUsage = networkInstanceNames.Select(instance =>
                    new PerformanceCounter("Network Interface", "Bytes Total/sec", instance)).ToList();
            }
            catch (Exception e)
            {
                Trace.WriteLine(e);
            }
        }

        /// <summary>
        /// Trimmed down function that calls every performance counter once since first time call will always return 0
        /// </summary>
        private static void InitializeCounters()
        {
            try
            {
                DiskUsage.ForEach(disk => { disk.NextValue(); });
                AvailableMem.NextValue();
                CpuUsage.NextValue();
                NetworkBandwidth.ForEach(network => network.NextValue());
                NetworkUsage.ForEach(network => network.NextValue());
            }
            catch (Exception e)
            {
                Trace.WriteLine(e);
            }
        }

        public static async Task<TelemetryStats> CollectTelemetryStats()
        {
            try
            {
                return await Task.Run(async () =>
                {
                    CurrentStats = new TelemetryStats()
                    {
                        DiskUtilization = GetDiskUtilization(),
                        NetworkUtilization = GetNetworkUtilization(),
                        MemoryUtilization = GetMemoryUtilization(),
                        GpuUtilization = await GetGpuUtilization(),
                        CpuUtilization = GetCpuUtilization(),
                        GpuTemps = GetGpuTemps()
                    };
                    return CurrentStats;
                });
            }
            catch (Exception e)
            {
                Trace.WriteLine(e);
                return default;
            }
        }

        public static TelemetryStats CurrentStats { get; set; }

        private static BindingList<TelemetryRecording> GetNetworkUtilization()
        {
            try
            {
                var bandwidthList = from bandwidthCounter in NetworkBandwidth
                    join usageCounter in NetworkUsage on bandwidthCounter.InstanceName equals usageCounter.InstanceName
                    let utilization = CalculateNetworkUtilization(bandwidthCounter, usageCounter)
                    where !double.IsNaN(utilization)
                    select new TelemetryRecording()
                    {
                        Instance = bandwidthCounter.InstanceName,
                        Value = utilization
                    };

                return new BindingList<TelemetryRecording>(bandwidthList.ToList());
            }
            catch (Exception e)
            {
                return default;
            }
        }

        private static BindingList<TelemetryRecording> GetDiskUtilization()
        {
            try
            {
                var diskStats = new BindingList<TelemetryRecording>();

                DiskUsage.ForEach(disk =>
                {
                    try
                    {
                        var diskStat = new TelemetryRecording
                        {
                            Instance = Regex.Replace(disk.InstanceName, @"[\d-]", string.Empty).Trim(),
                            Value = disk.NextValue()
                        };
                        diskStats.Add(diskStat);
                    }
                    catch
                    {
                        //Log error here
                    }
                });

                return diskStats;
            }
            catch (Exception e)
            {
                return default;
            }
        }

        private static TelemetryRecording GetCpuUtilization()
        {
            try
            {
                return new TelemetryRecording()
                {
                    Instance = "",
                    Value = CpuUsage.NextValue()
                };
            }
            catch
            {
                //Debug log goes here
                return null;
            }
        }

        private static async Task<BindingList<TelemetryRecording>> GetGpuUtilization()
        {
            try
            {
                switch (GpuType)
                {
                    case HardwareType.Nvidia:
                        return await NvidiaGpu.GetGpuUtilization();
                    case HardwareType.Amd:
                        break;
                    case HardwareType.Unknown:
                        break;
                    default:
                        break;
                }
            }
            catch
            {
                //log goes here
                
            }
            
            return new BindingList<TelemetryRecording>();
        }

        private static BindingList<TelemetryRecording> GetGpuTemps()
        {
            try
            {
                switch (GpuType)
                {
                    case HardwareType.Nvidia:
                        return NvidiaGpu.GetGpuTemp();
                    case HardwareType.Amd:
                        break;
                    case HardwareType.Unknown:
                        break;
                    default:
                        break;
                }
            }
            catch
            {
                //log goes here

            }

            return new BindingList<TelemetryRecording>();
        }

        private static TelemetryRecording GetMemoryUtilization()
        {
            try
            {
                return new TelemetryRecording()
                {
                    Instance = "",
                    Value = ((TotalMemory - AvailableMem.NextValue()) / TotalMemory) * 100
                };
            }
            catch
            {
                //Debug log goes here
                return null;
            }
        }

        private static double CalculateNetworkUtilization(PerformanceCounter bandwidth, PerformanceCounter usage)
        {
            try
            {
                var totalBytesPerSecVal = usage.NextValue();
                var bandwidthVal = bandwidth.NextValue();

                //If bandwidth is 0 the network is not active, so we don't want to add it to our list of networks
                //We can't trust a return val of 0 since the actual utilization can be 0.
                return bandwidthVal > 0 ? (totalBytesPerSecVal * 8 / bandwidthVal) * 100 : double.NaN;
            }
            catch
            {
                return double.NaN;
            }
            
        }

        private static void GetGpuType()
        {
            try
            {
                NVIDIA.Initialize();
                GpuType = HardwareType.Nvidia;
            }
            catch
            {
                // ignored
            }
        }
    }
}
