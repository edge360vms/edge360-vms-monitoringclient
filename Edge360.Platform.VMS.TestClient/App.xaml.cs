﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using Telerik.Windows.Controls;

namespace Edge360.Platform.VMS.TestClient
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public static Color GetColorFromString(string color)
        {
            try
            {
                var convertedColor = ColorConverter.ConvertFromString(color);
                if (convertedColor != null)
                {
                    return (Color) convertedColor;
                }
            }
            catch (Exception)
            {
            }

            return default;
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            StyleManager.ApplicationTheme = new GreenTheme();
            GreenPalette.Palette.AccentHighColor = GetColorFromString("#FF3B3B4C");
            GreenPalette.Palette.AccentLowColor = GetColorFromString("#FF7D86B1");
            GreenPalette.Palette.AlternativeColor = GetColorFromString("#FF1D1E21");
            GreenPalette.Palette.BasicColor = GetColorFromString("#FF474747");
            GreenPalette.Palette.ComplementaryColor = GetColorFromString("#FF444446");
            GreenPalette.Palette.HighColor = GetColorFromString("#FF131313");
            GreenPalette.Palette.LowColor = GetColorFromString("#FF343434");
            GreenPalette.Palette.MainColor = GetColorFromString("#FF1B1B1F");
            GreenPalette.Palette.MarkerColor = GetColorFromString("#FFF1F1F1");
            GreenPalette.Palette.MouseOverColor = GetColorFromString("#FF4B4B5F");
            GreenPalette.Palette.PrimaryColor = GetColorFromString("#272a31"); //GetColorFromString("#FF2B2C2E");
            GreenPalette.Palette.SelectedColor = GetColorFromString("#FFFFFFFF");
            GreenPalette.Palette.SemiAccentLowColor = GetColorFromString("#596560b3");
            GreenPalette.Palette.StrongColor = GetColorFromString("#FF646464");
            GreenPalette.Palette.ValidationColor = GetColorFromString("#FFE60000");
            GreenPalette.Palette.DisabledOpacity = 0.2;
            GreenPalette.Palette.ReadOnlyOpacity = 1;
            base.OnStartup(e);
        }
    }
}
