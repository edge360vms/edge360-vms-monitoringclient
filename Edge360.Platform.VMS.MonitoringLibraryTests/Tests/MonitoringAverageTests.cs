﻿using System;
using System.ComponentModel;
using System.Linq;
using Edge360.Platform.VMS.MonitoringLibrary.Models;
using Edge360.Platform.VMS.MonitoringLibraryTests.Resources;
using NUnit.Framework;
using NUnit.Framework.Internal;

namespace Edge360.Platform.VMS.MonitoringLibraryTests.Tests
{
    [TestFixture]
    public class MonitoringAverageTests
    {
        [Test]
        public void DiskAverageTest()
        {
            var valueSolutionPair = AverageTestFactory.DiskTestValuePair();
            var queue = valueSolutionPair.Key;
            var solution = valueSolutionPair.Value;

            var avg = queue.GetAverageStats();

            Assert.That(StatsEqual(avg.DiskUtilization, solution));
        }

        [Test]
        public void MemoryAverageTest()
        {
            var valueSolutionPair = AverageTestFactory.MemoryTestValuePair();
            var queue = valueSolutionPair.Key;
            var solution = valueSolutionPair.Value;

            var avg = queue.GetAverageStats();

            Assert.That(StatsEqual(avg.MemoryUtilization, solution));
        }

        /// <summary>
        /// A Telemetry queue with no entries
        /// </summary>
        [Test]
        public void EmptyQueueTest()
        {
            var queue = AverageTestFactory.EmptyQueue();
            var avg = queue.GetAverageStats();
            Assert.That(StatsBlank(avg));
        }

        /// <summary>
        /// A telemetry queue with a single telemetry recording (no members initialized)
        /// </summary>
        [Test]
        public void BlankRecordingQueueTest()
        {
            var queue = AverageTestFactory.BlankObjectInQueue();
            var avg = queue.GetAverageStats();
            Assert.That(StatsBlank(avg));
        }

        /// <summary>
        /// A telemetry queue with a single telemetry recording (only Disk Utilization initialized to a blank list)
        /// </summary>
        [Test]
        public void EmptyTelemetryListTest()
        {
            var queue = AverageTestFactory.EmptyList();
            var avg = queue.GetAverageStats();
            Assert.That(StatsBlank(avg));
        }

        /// <summary>
        /// A telemetry queue with a single telemetry recording (only Memory Utilization initialized to null)
        /// </summary>
        [Test]
        public void NullTelemetryRecordingTest()
        {
            var queue = AverageTestFactory.NullRecording();
            var avg = queue.GetAverageStats();
            Assert.That(StatsBlank(avg));
        }

        private bool StatsEqual(BindingList<TelemetryRecording> stats, BindingList<TelemetryRecording> solution)
        {
            foreach (var stat in stats)
            {
                var corr = solution.FirstOrDefault(x => x.Instance.Equals(stat.Instance));

                if (stat == null || !corr.Equals(stat))
                {
                    return false;
                }
            }

            return true;
        }

        private bool StatsEqual(TelemetryRecording stat, TelemetryRecording solution)
        {
            return stat != null && stat.Equals(solution);
        }

        private bool StatsBlank(TelemetryStats stats)
        {
            return stats.DiskUtilization.Count == 0
                && stats.GpuUtilization.Count == 0
                && stats.GpuTemps.Count == 0
                && stats.NetworkUtilization.Count == 0
                && stats.CpuUtilization == null
                && stats.MemoryUtilization == null;
        }
    }
}
