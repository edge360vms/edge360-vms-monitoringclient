﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Edge360.Platform.VMS.MonitoringLibrary.Models;
using NUnit.Framework.Constraints;

namespace Edge360.Platform.VMS.MonitoringLibraryTests.Resources
{
    public static class AverageTestFactory
    {
        public static KeyValuePair<TelemetryQueue<TelemetryStats>, BindingList<TelemetryRecording>> DiskTestValuePair()
        {
            var queue = new TelemetryQueue<TelemetryStats>(8);

            var diskStats1 = new BindingList<TelemetryRecording>()
            {
                new TelemetryRecording()
                {
                    Instance = "C",
                    Value = 40
                },
                new TelemetryRecording()
                {
                    Instance = "D",
                    Value = 50
                }
            };

            var diskStats2 = new BindingList<TelemetryRecording>()
            {
                new TelemetryRecording()
                {
                    Instance = "C",
                    Value = 20
                },
                new TelemetryRecording()
                {
                    Instance = "D",
                    Value = 10
                }
            };

            var diskStats3 = new BindingList<TelemetryRecording>();

            var diskStats4 = new BindingList<TelemetryRecording>()
            {
                new TelemetryRecording()
                {
                    Instance = "C",
                    Value = 10
                },
                null
            };

            var diskStats5 = new BindingList<TelemetryRecording>()
            {
                null,
                new TelemetryRecording()
                {
                    Instance = "D",
                    Value = 0
                }
            };

            var diskStats6 = new BindingList<TelemetryRecording>()
            {
                new TelemetryRecording()
                {
                    Instance = "D",
                    Value = 20
                }
            };

            var diskStats7 = new BindingList<TelemetryRecording>()
            {
                new TelemetryRecording()
                {
                    Instance = "C",
                    Value = 90
                }
            };

            var diskStats8 = new BindingList<TelemetryRecording>()
            {
                new TelemetryRecording()
                {
                    Instance = "D",
                    Value = 0
                }
            };

            queue.Enqueue(new TelemetryStats()
            {
                DiskUtilization = diskStats1
            });

            queue.Enqueue(new TelemetryStats()
            {
                DiskUtilization = diskStats2
            });

            queue.Enqueue(new TelemetryStats()
            {
                DiskUtilization = diskStats3
            });

            queue.Enqueue(new TelemetryStats()
            {
                DiskUtilization = diskStats4
            });

            queue.Enqueue(new TelemetryStats()
            {
                DiskUtilization = diskStats5
            });

            queue.Enqueue(new TelemetryStats()
            {
                DiskUtilization = diskStats6
            });

            queue.Enqueue(new TelemetryStats()
            {
                DiskUtilization = diskStats7
            });

            queue.Enqueue(new TelemetryStats()
            {
                DiskUtilization = diskStats8
            });

            var solution = new BindingList<TelemetryRecording>()
            {
                new TelemetryRecording()
                {
                    Instance = "C",
                    Value = 40
                },
                new TelemetryRecording()
                {
                    Instance = "D",
                    Value = 16
                }
            };

            return new KeyValuePair<TelemetryQueue<TelemetryStats>,BindingList<TelemetryRecording>>(queue, solution);
        }

        public static KeyValuePair<TelemetryQueue<TelemetryStats>, TelemetryRecording> MemoryTestValuePair()
        {
            var queue = new TelemetryQueue<TelemetryStats>(6);
            var memoryUtil1 = new TelemetryRecording()
            {
                Instance = "",
                Value = 45
            };

            var memoryUtil2 = new TelemetryRecording()
            {
                Instance = "",
                Value = 32
            };

            var memoryUtil3 = new TelemetryRecording()
            {
                Instance = "",
                Value = 15
            };

            TelemetryRecording memoryUtil4 = null;

            var memoryUtil5 = new TelemetryRecording()
            {
                Instance = "",
                Value = 35
            };

            var memoryUtil6 = new TelemetryRecording()
            {
                Instance = "",
                Value = 25
            };

            queue.Enqueue(new TelemetryStats()
            {
                MemoryUtilization = memoryUtil1
            });

            queue.Enqueue(new TelemetryStats()
            {
                MemoryUtilization = memoryUtil2
            });

            queue.Enqueue(new TelemetryStats()
            {
                MemoryUtilization = memoryUtil3
            });

            queue.Enqueue(new TelemetryStats()
            {
                MemoryUtilization = memoryUtil4
            });

            queue.Enqueue(new TelemetryStats()
            {
                MemoryUtilization = memoryUtil5
            });

            queue.Enqueue(new TelemetryStats()
            {
                MemoryUtilization = memoryUtil6
            });

            var solution = new TelemetryRecording()
            {
                Instance = "",
                Value = 30.4
            };

            return new KeyValuePair<TelemetryQueue<TelemetryStats>, TelemetryRecording>(queue, solution);
        }

        public static TelemetryQueue<TelemetryStats> EmptyQueue()
        {
            return new TelemetryQueue<TelemetryStats>(5);
        }

        public static TelemetryQueue<TelemetryStats> BlankObjectInQueue()
        {
            var queue = new TelemetryQueue<TelemetryStats>(5);
            queue.Enqueue(new TelemetryStats());

            return queue;
        }

        public static TelemetryQueue<TelemetryStats> EmptyList()
        {
            var queue = new TelemetryQueue<TelemetryStats>(5);
            queue.Enqueue(new TelemetryStats()
            {
                DiskUtilization = new BindingList<TelemetryRecording>()
            });

            return queue;
        }

        public static TelemetryQueue<TelemetryStats> NullRecording()
        {
            var queue = new TelemetryQueue<TelemetryStats>(5);
            queue.Enqueue(new TelemetryStats()
            {
                MemoryUtilization = null
            });

            return queue;
        }
    }
}
